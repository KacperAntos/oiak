.data
STDIN 		= 0
STDOUT 		= 1
SYSWRITE 	= 1
SYSREAD 	= 0
SYSOPEN 	= 2
SYSCLOSE 	= 3
SYSEXIT 	= 60
EXIT_SUCCESS 	= 0
BUFLEN 		= 1024
RDONLY 		= 0

buferror: 	.ascii	"Blad odczytu liczby!\n"
buferror_len = 	.-buferror
newline: 	.ascii 	"\n"
newline_len = 	.-newline

file1: 		.asciz 	"liczba1.txt"
file2: 		.asciz 	"liczba2.txt"

fileSum: 	.asciz 	"wynik.txt"

.bss
.comm textin1, 	1024
.comm textin2, 	1024
.comm number1, 	1024
.comm number2, 	1024
.comm result, 	1025
.comm textout, 	1026
.comm octal,	1400

.text
.globl _start

_start:
// OTWARCIE PLIKU WEJSCIOWEGO - FILE1
movq $SYSOPEN, 	%rax
movq $file1, 	%rdi
movq $RDONLY, 	%rsi		#// Flaga - tylko do odczytu
movq $1, 	%rdx
syscall
push %rax			#// Zapis file descriptora znajdujacego sie w %rax

movq $STDIN, 	%rax
pop 		%rdi
movq $textin1, 	%rsi		#// Zapis liczby z pliku do $textin
movq $BUFLEN, 	%rdx
syscall

// ZAMIANA NA LICZBE W KONWENCJI "Little Endion"
movq %rax, 	%r8
movq $0, 	%r9
movq $0, 	%r10
dec 		%r10		#// Ustawienie r10 na -1
movq $0, 	%rcx
dec %r8

keep_reading1:			#// Zczytanie znakow od tylu
movq $0, 	%r12
dec %r8
cmp %r10, 	%r8
je end_loop1_early
movb textin1(, %r8, 1), %bl

cmp $0x20, %bl
jl zero_b1
cmp $0x30, %bl
jl display_error
cmp $0x39, %bl
jg letter_b1
sub $48, %bl
jmp end_b1
letter_b1:
cmp $0x41, %bl
jl display_error
cmp $0x46, %bl
jg display_error
sub $55, %bl
jmp end_b1
zero_b1:
movb $0, %bl
end_b1:
add %rbx, %r12

dec %r8
cmp %r10, %r8
je end_loop1_late
movb textin1(, %r8, 1), %cl

cmp $0x20, %cl
jl zero_c1
cmp $0x30, %cl
jl display_error
cmp $0x39, %cl
jg letter_c1
sub $48, %cl
jmp end_c1
letter_c1:
cmp $0x41, %cl
jl display_error
cmp $0x46, %cl
jg display_error
sub $55, %cl
jmp end_c1
zero_c1:
movb $0, %cl
end_c1:

movq $16, %rax
mul %rcx
add %rax, %r12

mov %r12, number1(, %r9, 1)
inc %r9

jmp keep_reading1

end_loop1_late:
mov %r12, number1(, %r9, 1)
end_loop1_early:

// OTWARCIE PLIKU WEJSCIOWEGO - FILE2
movq $SYSOPEN, 	%rax
movq $file2, 	%rdi
movq $RDONLY, 	%rsi
movq $1, 	%rdx
syscall
push %rax

movq $STDIN, 	%rax
pop 		%rdi
movq $textin2, 	%rsi
movq $BUFLEN, 	%rdx
syscall

// ZAMIANA NA LICZBE W KONWENCJI "Little Endion"
movq %rax, 	%r8
movq $0, 	%rcx
movq $0, 	%r9
movq $0, 	%r10
dec %r10
dec %r8

keep_reading2:
movq $0, 	%r12
dec %r8
cmp %r10, %r8
je end_loop2_early
movb textin2(, %r8, 1), %bl

cmp $0x20, %bl
jl zero_b2
cmp $0x30, %bl
jl display_error
cmp $0x39, %bl
jg letter_b2
sub $48, %bl
jmp end_b2
letter_b2:
cmp $0x41, %bl
jl display_error
cmp $0x46, %bl
jg display_error
sub $55, %bl
jmp end_b2
zero_b2:
movb $0, %bl
end_b2:
add %rbx, %r12

dec %r8
cmp %r10, %r8
je end_loop2_late
movb textin2(, %r8, 1), %cl

cmp $0x20, %cl
jl zero_c2
cmp $0x30, %cl
jl display_error
cmp $0x39, %cl
jg letter_c2
sub $48, %cl
jmp end_c2
letter_c2:
cmp $0x41, %cl
jl display_error
cmp $0x46, %cl
jg display_error
sub $55, %cl
jmp end_c2
zero_c2:
movb $0, %cl
end_c2:

movq $16, %rax
mul %rcx
add %rax, %r12

mov %r12, number2(, %r9, 1)
inc %r9
jmp keep_reading2

end_loop2_late:
mov %r12, number2(, %r9, 1)
end_loop2_early:

// KONIEC ZAMIANY - LICZBY W KONWENCJI "Little Endion" SA W BUFORACH
// DODAWANIE DUZYCH LICZB

movq $0, 	%r9		#// Wskaznik dodawanych bajtow
movq $0, 	%r10		#// Wskaznik do zapisu
movq $0, 	%rax
movq $0, 	%rbx
movq $0, 	%rcx
movq $16, 	%rdi
clc

keep_adding:
movb number1(, %r9, 1), %al	#// Zczytanie znaku liczby do rejestru al
movb number2(, %r9, 1), %bl	#// Zczytanie znaku liczby do rejestru bl

cmp $0, %cl			#// Sprawdzanie ustawienia flagi
jne flag_to_one
clc				#// Ustawienie flagi na 0
jmp add_continue

flag_to_one:
stc				#// Ustawienie flagi na 1

add_continue:
adc %al, %bl
setc %cl
movb %bl, result(, %r9, 1)

movq $0, %rdx
movq $0, %rax
movb %bl, %al
div %rdi

cmp $9, %dl
jg letter_d_textout
add $48, %dl
jmp write_d_to_textout
letter_d_textout:
add $55, %dl
write_d_to_textout:
movb %dl, textout(, %r10, 1)
inc %r10

cmp $9, %al
jg letter_a_textout
add $48, %al
jmp write_a_to_textout
letter_a_textout:
add $55, %al
write_a_to_textout:
movb %al, textout(, %r10, 1)
inc %r10

inc %r9
cmp $BUFLEN, %r10
jl keep_adding

movb %cl, result(, %r9, 1)	#// Dopisanie obecnego stanu flagi na koniec wyniku
movb $'\n', textout(, %r10, 1)

// WYNIK DODAWANIA JEST W BUFORZE RESULT
// ZAMIANA NA SYSTEM OSEMKOWY

movq $0, 	%r9		#// Licznik do wyniku w systemie szesnastkowym
movq $0, 	%r10		#// Licznik do wyniku w systemie osemkowym
movq $8, 	%r11		#// Podstawa nowego systemu
movq $0, 	%rax
movq $16, 	%r14		#// Waga drugiej wartosci
movq $256, 	%r15		#// Waga trzeciej wartosci

convert_to_octal:
movb textout(, %r9, 1), %cl
cmp $0x39, %cl
jg octal_c_letter
sub $48, %cl
jmp octal_c_next
octal_c_letter:
sub $55, %cl
octal_c_next:
inc %r9
cmp $BUFLEN, %r9
je quit_on_c

movb textout(, %r9, 1), %al
cmp $0x39, %al
jg octal_b_letter
sub $48, %al
jmp octal_b_next
octal_b_letter:
sub $55, %al
octal_b_next:
mul %r14
movq %rax, %rbx
inc %r9
cmp $BUFLEN, %r9
je quit_on_b

movb textout(, %r9, 1), %al
cmp $0x39, %al
jg octal_a_letter
sub $48, %al
jmp octal_a_next
octal_a_letter:
sub $55, %al
octal_a_next:
mul %r15
inc %r9

jmp quit_on_a
quit_on_c:
movq $0, %rbx
quit_on_b:
movq $0, %rax
quit_on_a:
add %rbx, %rax
add %rcx, %rax

movq $0, %r12
horner:
movq $0, %rdx
div %r11
add $48, %dl
movb %dl, octal(, %r10, 1)
inc %r10
inc %r12
cmp $4, %r12
jne horner

cmp $BUFLEN, %r9
jne convert_to_octal
movb $'\n', octal(, %r10, 1)

// LICZBA W SYSTEMIE OSEMKOWYM JEST W BUFORZE

movq $SYSWRITE,		%rax
movq $STDOUT,		%rdi
movq $textout,		%rsi
movq $1025,		%rdx
syscall

movq $SYSWRITE, 	%rax
movq $STDOUT, 		%rdi
movq $newline, 		%rsi
movq $newline_len, 	%rdx
syscall

movq $SYSWRITE,		%rax
movq $STDOUT,		%rdi
movq $octal,		%rsi
movq $1400,		%rdx
syscall

// WIADOMOSC O BLEDNIE WPISANYCH DANYCH
jmp skip_error
display_error:
movq $SYSWRITE, 	%rax
movq $STDOUT, 		%rdi
movq $buferror, 	%rsi
movq $buferror_len, 	%rdx
syscall
skip_error:

// WYJSCIE Z APLIKACJI
quit_app:
movq $SYSEXIT, 		%rax
movq $EXIT_SUCCESS, 	%rdi
syscall
