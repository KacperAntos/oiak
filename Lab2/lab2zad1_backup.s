.data
STDIN = 0
STDOUT = 1
SYSWRITE = 1
SYSREAD = 0
SYSOPEN = 2
SYSCLOSE = 3
SYSEXIT = 60
EXIT_SUCCESS = 0
BUFLEN = 1024
RDONLY = 0
CRT_WRTONLY = 03101
END_OF_FILE = 0

buferror: .ascii "Blad odczytu liczby!\n"
buferror_len = .-buferror
newline: .ascii "\n"
newline_len = .-newline

file1: .asciz "liczba1.txt"
file2: .asciz "liczba2.txt"

fileSum: .asciz "wynik.txt"

.bss
.comm textin1, 1024
.comm textin2, 1024
.comm number1, 1024
.comm number2, 1024
.comm result, 1025
.comm textout, 1500

.text
.globl _start

_start:
// OTWARCIE PLIKU WEJSCIOWEGO - FILE1
movq $SYSOPEN, %rax
movq $file1, %rdi
movq $RDONLY, %rsi		#// Flaga - tylko do odczytu
movq $1, %rdx
syscall
push %rax			#// Zapis file descriptora znajdujacego sie w %rax

movq $STDIN, %rax
pop %rdi
movq $textin1, %rsi		#// Zapis liczby z pliku do $textin
movq $1024, %rdx
syscall

// ZAMIANA NA LICZBE W KONWENCJI "Little Endion"
movq %rax, %r8
movq $0, %r9
movq $0, %r10
dec %r10			#// Ustawienie r10 na -1
movq $0, %rcx
dec %r8

keep_reading1:			#// Zczytanie znakow od tylu
movq $0, %r12
dec %r8
cmp %r10, %r8
je end_loop1_early
movb textin1(, %r8, 1), %bl

cmp $0x30, %bl
jl zero_b1
cmp $0x39, %bl
jg letter_b1
sub $48, %bl
jmp end_b1
letter_b1:
sub $55, %bl
jmp end_b1
zero_b1:
movb $0, %bl
end_b1:

movq $16, %rax
mul %rbx
add %rax, %r12

dec %r8
cmp %r10, %r8
je end_loop1_late
movb textin1(, %r8, 1), %cl

cmp $0x30, %cl
jl zero_c1
cmp $0x39, %cl
jg letter_c1
sub $48, %cl
jmp end_c1
letter_c1:
sub $55, %cl
jmp end_c1
zero_c1:
movb $0, %cl
end_c1:

add %rcx, %r12

mov %r12, number1(, %r9, 1)
inc %r9

jmp keep_reading1

end_loop1_late:
movb $0, %cl
add %rcx, %r12
mov %r12, number1(, %r9, 1)
end_loop1_early:

// OTWARCIE PLIKU WEJSCIOWEGO - FILE2
movq $SYSOPEN, %rax
movq $file2, %rdi
movq $RDONLY, %rsi
movq $1, %rdx
syscall
push %rax

movq $STDIN, %rax
pop %rdi
movq $textin2, %rsi
movq $1024, %rdx
syscall

// ZAMIANA NA LICZBE W KONWENCJI "Little Endion"
movq %rax, %r8
movq $0, %rcx
movq $0, %r9
movq $0, %r10
dec %r10
dec %r8

keep_reading2:
movq $0, %r12
dec %r8
cmp %r10, %r8
je end_loop2_early
movb textin2(, %r8, 1), %bl

cmp $0x30, %bl
jl zero_b2
cmp $0x39, %bl
jg letter_b2
sub $48, %bl
jmp end_b2
letter_b2:
sub $55, %bl
jmp end_b2
zero_b2:
movb $0, %bl
end_b2:

movq $16, %rax
mul %rbx
add %rax, %r12

dec %r8
cmp %r10, %r8
je end_loop2_late
movb textin2(, %r8, 1), %cl

cmp $0x30, %cl
jl zero_c2
cmp $0x39, %cl
jg letter_c2
sub $48, %cl
jmp end_c2
letter_c2:
sub $55, %cl
jmp end_c2
zero_c2:
movb $0, %cl
end_c2:

add %rcx, %r12

mov %r12, number2(, %r9, 1)
inc %r9

jmp keep_reading2

end_loop2_late:
movb $0, %cl
add %rcx, %r12
mov %r12, number2(, %r9, 1)
end_loop2_early:

//movq $SYSWRITE, %rax
//movq $STDOUT, %rdi
//movq $number1, %rsi
//movq $1024, %rdx
//syscall

//movq $SYSWRITE, %rax
//movq $STDOUT, %rdi
//movq $newline, %rsi
//movq $newline_len, %rdx
//syscall

//movq $SYSWRITE, %rax
//movq $STDOUT, %rdi
//movq $number2, %rsi
//movq $1024, %rdx
//syscall

// KONIEC ZAMIANY - LICZBY W KONWENCJI "Little Endion" SA W BUFORACH
// DODAWANIE DUZYCH LICZB

movq $0, %r9
movq $0, %rax
movq $0, %rbx
movq $0, %rcx
clc

keep_adding:
movb number1(, %r9, 1), %al	#// Zczytanie znaku liczby do rejestru al
movb number2(, %r9, 1), %bl	#// Zczytanie znaku liczby do rejestru bl

cmp $0, %cl			#// Sprawdzanie ustawienia flagi
jne flag_to_one
clc				#// Ustawienie flagi na 0
jmp add_continue

flag_to_one:
stc				#// Ustawienie flagi na 1

add_continue:
adc %al, %bl
setc %cl
movb %bl, result(, %r9, 1)

cmp $9, %bl
jg letter_textout
add $48, %bl
jmp write_to_textout

letter_textout:
add $55, %bl

write_to_textout:
movb %bl, textout(, %r9, 1)

inc %r9
cmp $1024, %r9
jl keep_adding

movb %cl, result(, %r9, 1)	#// Dopisanie obecnego stanu flagi na koniec wyniku

// WYNIK DODAWANIA JEST W BUFORZE RESULT
movq $SYSWRITE, %rax
movq $STDOUT, %rdi
movq $newline, %rsi
movq $newline_len, %rdx
syscall

movq $SYSWRITE, %rax
movq $STDOUT, %rdi
movq $textout, %rsi
movq $1025, %rdx
syscall

// WIADOMOSC O BLEDNIE WPISANYCH DANYCH
jmp skip_error
display_error:
movq $SYSWRITE, %rax
movq $STDOUT, %rdi
movq $buferror, %rsi
movq $buferror_len, %rdx
syscall
skip_error:

// WYJSCIE Z APLIKACJI
quit_app:
movq $SYSEXIT, %rax
movq $EXIT_SUCCESS, %rdi
syscall
