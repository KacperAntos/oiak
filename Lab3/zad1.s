.data
STDIN		= 0
STDOUT		= 1
SYSREAD		= 0
SYSWRITE	= 1
SYSEXIT		= 60
EXIT_SUCCESS	= 0
BUFLEN		= 512

charInfo:	.ascii	"Wprowadz ciag znakow:\n"
charInfo_len 	= .-charInfo
noChainInfo:	.ascii 	"Nie znaleziono ciagu 'aaa'\n"
noChainInfo_len = .-noChainInfo

.bss
.comm textin,	512
.comm temp,	512
.comm textout,	512

.text
.globl _start

_start:
movq	$SYSWRITE,	%rax
movq	$STDOUT,	%rdi
movq	$charInfo,	%rsi
movq	$charInfo_len,	%rdx
syscall

movq	$SYSREAD,	%rax
movq	$STDIN,		%rdi
movq	$textin, 	%rsi
movq	$BUFLEN, 	%rdx
syscall

call findAAA

cmp	$0,	%r9
jl	no_chain

movq	%r9,	%rax
movq	$10,	%r10
movq	$0,	%r11
convert_to_ascii:
movq	$0,	%rdx
div	%r10
add	$48,	%rdx
movb	%dl,	temp(, %r11, 1)
inc	%r11

cmp	$0,	%rax
jne	convert_to_ascii
movb	$'\n',	textout(, %r11, 1)

movq	$0,	%r12
flip:
dec	%r11
movb	temp(, %r11, 1),	%al
movb	%al,	textout(, %r12, 1)
inc	%r12
cmp	$0,	%r11
jne flip

movq	$SYSWRITE,	%rax
movq	$STDOUT,	%rdi
movq	$textout,	%rsi
movq	$BUFLEN,	%rdx
syscall
jmp quit_app

no_chain:
movq	$SYSWRITE,	%rax
movq	$STDOUT,	%rdi
movq	$noChainInfo,	%rsi
movq	$noChainInfo_len,%rdx
syscall

quit_app:
movq $SYSEXIT,		%rax
movq $EXIT_SUCCESS,	%rsi
syscall

// FUNKCJA

findAAA:		#//r9 - wynik
movq	$0,	%r9
movq	$0,	%rdx

loop:
movb	textin(, %r9, 1),	%dl
cmp	$97,	%dl
jne	continue

inc	%r9
cmp	$BUFLEN,%r9
je	return_minus_one
movb	textin(, %r9, 1),	%dl
cmp	$97,	%dl
jne	continue

inc	%r9
cmp	$BUFLEN,%r9
je	return_minus_one
movb	textin(, %r9, 1),	%dl
cmp	$97,	%dl
je	found_value

continue:
inc	%r9
cmp	$BUFLEN,%r9
jne	loop

return_minus_one:
movq	$0,	%r9
dec	%r9
jmp end_func

found_value:
sub $2, %r9

end_func:
ret
