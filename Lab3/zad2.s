.data
STDIN		= 0
STDOUT		= 0
SYSREAD 	= 0
SYSWRITE	= 1
SYSEXIT		= 60
EXIT_SUCCESS	= 0
BUFLEN		= 512

instruction:	.ascii	"Podaj indeks wyrazu ciagu:\n"
instruction_len = .-instruction

.bss
.comm	index,	512
.comm	temp,	512
.comm	textout,512

.text
.globl _start

_start:
movq	$SYSWRITE,	%rax
movq	$STDOUT,	%rdi
movq	$instruction,	%rsi
movq	$instruction_len,%rdx
syscall

movq	$SYSREAD,	%rax
movq	$STDIN,		%rdi
movq	$index,		%rsi
movq	$BUFLEN,	%rdx
syscall

movq	$0,	%r8
movq	$10,	%r10
movq	%rax,	%rcx
dec	%rcx
movq	$0,	%rax

convert_index_to_number:
mul	%r10
movb	index(, %r8, 1),%bl
sub	$48,	%bl
add	%rbx,	%rax
inc	%r8
cmp	%r8,	%rcx
jne	convert_index_to_number
movq	%rax,	%r9

// Wartosc indeksu znajduje sie w %r9

movq	$5,	%rdx
push	%rdx
movq	$2,	%rdx
push	%rdx
push	%r9
call	GetValueByStack
pop	%rcx

movq	$5,	%r11
movq	$2,	%r12
call	GetValueByRegistry

quit_app:
movq	$SYSEXIT,	%rax
movq	$EXIT_SUCCESS,	%rsi
syscall

// FUNKCJE

// Poprzez rejestr
GetValueByRegistry:	#// Indeks w %r9, wyrazy w %r11 i %r12, wynik w %r10
cmp	$0,	%r9
je	return_registry

movq	%r12,	%rax
movq	$3,	%rdx
mul	%rdx
movq	%rax,	%r13

movq	%r11,	%rax
movq	$2,	%rdx
mul	%rdx
sub	%rax,	%r13

dec	%r9
movq	%r12,	%r11
movq	%r13,	%r12
call	GetValueByRegistry

return_registry:
movq	%r11,	%r10
ret

// Poprzez stos
GetValueByStack:	#// Indeks w %r9
push	%rbp
mov	%rsp,	%rbp

mov	32(%rbp),	%r11
mov	24(%rbp),	%r12
mov	16(%rbp),	%r14

cmp	$0,	%r14
je	return_stack

movq	%r12,	%rax
movq	$3,	%rdx
mul	%rdx
movq	%rax,	%r13

movq	%r11,	%rax
movq	$2,	%rdx
mul	%rdx
sub	%rax,	%r13

dec	%r14
push	%r12
push	%r13
push	%r14
call	GetValueByStack

return_stack:
movq	%r11,	16(%rbp)
mov	%rbp,	%rsp
pop	%rbp
ret
