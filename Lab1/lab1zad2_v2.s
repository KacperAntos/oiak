.data
STDIN = 0
STDOUT = 1
SYSWRITE = 1
SYSREAD = 0
SYSEXIT = 60
EXIT_SUCCESS = 0
BUFLEN = 512

buferror: .ascii "Wprowadzona liczba jest niepoprawna!\n"
buferror_len = .-buferror

.bss
.comm textin, 512
.comm textout, 512

.text
.globl _start

_start:
// WCZYTANIE LICZBY
mov $SYSREAD, %rax
mov $STDIN, %rdi
mov $textin, %rsi
mov $BUFLEN, %rdx
syscall

// ZMIENNE POTRZEBNE DO ZAMIANY LICZB
movq $0, %rbp			# Miejsce na wartosc wprowadzonej liczby
movq $3, %rcx			# Podstawa systemu do pozniejszej zamiany
movq %rax, %rsp			# Backup rejestru rax

// ROZPOCZECIE SPRAWDZANIA I ZAMIANY LICZBY
check:
movb textin(, %rdi, 1), %bl	# Obecnie sprawdzana cyfra
// Znaki specjalne
cmp $0x19, %bl
jl skip
cmp $0x30, %bl
jl display_error
cmp $0x32, %bl
jg display_error
// Wprowadzona liczba jest porawna
// Zamiana z kodu ASCII na liczbe i wyliczenie potegi
sub $48, %bl
movq $1, %rax			# Miejsce na wyliczenie mnoznika
movq %rdi, %rsi			# Kopia obecnie sprawdzanego indeksu

jmp pow_start
pow:
dec %rsi
mul %rcx			# Ponowne mnozenie x3 -> 1, 3, 9 etc.
pow_start:
cmp $0, %rsi
jne pow

mul %bl
add %rax, %rbp
skip:
inc %rdi
cmp %rsp, %rdi			# Czy cala liczba zostala zczytana
jl check			# Jesli nie, czytaj nastepna
// KONIEC SPRAWDZANIA I ZAMIANY LICZBY - WARTOSC LICZBY ZNAJDUJE SIE W REJESTRZE rbp

// WYLICZENIE REPREZENTACJI LICZBY W SYSTEMIE O PODSTAWIE 9
movq %rbp, %rax			# Przeniesienie wartosci do rejestru rax
movq $0, %rsi
movq $9, %rcx			# Podstawa nowego systemu

horner:
movq $0, %rdx			# Czyszczenie rejestru rdx przed mnozeniem
div %rcx			# Dzielenie przez podstawe systemu
add $48, %rdx			# Zamiana reszty z dzielenia na kod ASCII
movb %dl, textout(, %rsi, 1)	# Zapis koncowki rejestru rdx do tekstu wyjsciowego
inc %rsi
cmp $0, %rax			# Czy zostala juz zamieniona cala liczba
jne horner			# Jesli nie - powtarzamy dzielenie

// WYPISANIE ZAMIENIONEJ LICZBY
wypisz:
movb $'\n', textout(, %rsi, 1)
movq $SYSWRITE, %rax
movq $STDOUT, %rdi
movq $textout, %rsi
movq $BUFLEN, %rdx
syscall

// WIADOMOSC O BLEDNIE WPISANYCH DANYCH
jmp quit_app
display_error:
movq $SYSWRITE, %rax
movq $STDOUT, %rdi
movq $buferror, %rsi
movq $buferror_len, %rdx
syscall

// WYJSCIE Z APLIKACJI
quit_app:
movq $SYSEXIT, %rax
movq $EXIT_SUCCESS, %rdi
syscall
