.data
STDIN = 0
STDOUT = 1
SYSWRITE = 1
SYSREAD = 0
SYSEXIT = 60
EXIT_SUCCESS = 0
BUFLEN = 512

buferror: .ascii "Wprowadzona liczba jest niepoprawna!\n"
buferror_len = .-buferror

.bss
.comm textin, 512
.comm textflip, 512
.comm textout, 512

.text
.globl _start

_start:
// Wczytanie liczby
mov $SYSREAD, %rax
mov $STDIN, %rdi
mov $textin, %rsi
mov $BUFLEN, %rdx
syscall

// Sprawdzenie poprawnosci i zamiana wczytanej liczby
movq $0, %rbx
movq $3, %rbp
movb $0x30, %bl
movq %rax, %rcx

sprawdz:
movb textin(, %rdi, 1), %bl
// Znak specjalny
cmp $0x19, %bl
jl skip
cmp $0x30, %bl
jl display_conv_error
cmp $0x32, %bl
jg display_conv_error

// Zamiana liczby
//sub %bh, %bl
sub $0x30, %bl

movq %rdi, %rsi
movq $1, %rax

jmp powstart
pow:
dec %rsi
mul %rbp
powstart:
cmp $0, %rsi
je endpow
jmp pow
endpow:
mul %bl
add %rbx, %rax

skip:
inc %rdi
cmp %rcx, %rdi
jl sprawdz
// Koniec petli - w rbx posiadamy wartosc wyliczana z definicji

// Wyliczanie wartosci w systemie o podst. 9 ze schematu hornera
movq %rbx, %rax
movq $0, %rsi
movq $9, %rbp
movq $0x30, %rcx

horner:
movq $0, %rdx
div %rbp
add %rdx, %rcx
movb %dl, textout(, %rsi, 1)
inc %rsi
cmp $0, %rax
jne horner

// z 111 -> 13	-> 41
// z 210 -> 5	-> 5

// Wypisanie zamienionej liczby
wypisz:
movb $'\n', textout(, %rsi, 1)
movq $SYSWRITE, %rax
movq $STDOUT, %rdi
movq $textout, %rsi
movq $BUFLEN, %rdx
syscall

// Wiadomosc o blednie wpisanych danych
jmp quit_app
display_conv_error:
movq $SYSWRITE, %rax
movq $STDOUT, %rdi
movq $buferror, %rsi
movq $buferror_len, %rdx
syscall

quit_app:
movq $SYSEXIT, %rax
movq $EXIT_SUCCESS, %rdi
syscall
