.data
SYSREAD = 0
SYSWRITE = 1
SYSEXIT = 60
SYSOUT = 1
SYSIN = 0
EXIT_SUCCESS = 0

NUMBER = 101
DIVIDER = 1

buftrue: .ascii "Liczba jest pierwsza\n"
buftrue_len = .-buftrue
buffalse: .ascii "Liczba nie jest pierwsza\n"
buffalse_len = .-buffalse

.text
.globl _start

_start:
movq $DIVIDER, %rcx
loop:
inc %rcx

// Dzielenie przez "DIVIDER"
movq $0, %rdx
movq $NUMBER, %rax
div %rcx

// Czy wynik ma reszte 0?
cmp $0, %rdx
je output_false

// Czy dzielnik jest ponizej polowy?
movq $0, %rdx
movq $2, %rbx
movq $NUMBER, %rax
div %rbx
inc %rax
cmp $DIVIDER, %rax
jb loop

output_true:
movq $SYSWRITE, %rax
movq $SYSOUT, %rdi
movq $buftrue, %rsi
movq $buftrue_len, %rdx
syscall
jmp stop_program

output_false:
movq $SYSWRITE, %rax
movq $SYSOUT, %rdi
movq $buffalse, %rsi
movq $buffalse_len, %rdx
syscall

stop_program:
movq $SYSEXIT, %rax
movq $EXIT_SUCCESS, %rdi
syscall
