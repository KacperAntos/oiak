.data
STDOUT		= 0
SYSREAD		= 0
SYSWRITE	= 1
SYSEXIT		= 60
EXIT_SUCCESS	= 0

WIDTH		= 6
HEIGHT		= 7

star:		.ascii	"*"
star_len	= .-star
enter:		.ascii "\n"
enter_len	= .-enter
space:		.ascii " "
space_len	= .-space

.text
.globl _start

_start:

movq	$0,	%r9
movq	$0,	%r12

line:
movq	$0,	%r9
cmp	$0,	%r12
je	char

movq	$0,	%r13
spaces:
call	AddSpace
inc	%r13
cmp	%r13,	%r12
jne 	spaces

char:
call	Write
inc	%r9
cmp	$WIDTH,	%r9
jne	char

call	NewLine
inc	%r12
cmp	$HEIGHT,%r12
jne	line

quit_app:
movq $SYSEXIT,	%rax
movq $EXIT_SUCCESS,%rdi
syscall

Write:
movq $SYSWRITE,	%rax
movq $STDOUT,	%rdi
movq $star,	%rsi
movq $star_len,	%rdx
syscall
ret

NewLine:
movq $SYSWRITE,	%rax
movq $STDOUT,	%rdi
movq $enter,	%rsi
movq $enter_len,%rdx
syscall
ret

AddSpace:
movq $SYSWRITE,	%rax
movq $STDOUT,	%rdi
movq $space,	%rsi
movq $space_len,%rdx
syscall
ret
