.data
SYSREAD		= 0

decimal:	.asciz	"%d"
float:		.asciz	"%f"
enter:		.asciz	"\n"

.bss
.comm	num_x,	8	#float
.comm	num_y,	8	#int

.text
.global main

main:
// ZCZYTANIE DWOCH LICZB
movq	$SYSREAD,	%rax
movq	$float,		%rdi
movq	$num_x,		%rsi
call	scanf

movq	$SYSREAD,	%rax
movq	$decimal,	%rdi
movq	$num_y,		%rsi
call	scanf

// WYWOLANIE FUNKCJI W C
movq	$1,	%rax
movq	$0,	%rdi
movq	$0,	%rbx

movq	num_x,	%xmm0
movq	num_y(, %rbx, 8),	%rdi
call	fun

cvtps2pd %xmm0,	%xmm0

// WYPISANIE WYNIKU
movq	$1,	%rax
movq	$float,	%rdi
subq	$8,	%rsp
call	printf
addq	$8,	%rsp

movq	$0,	%rax
movq	$enter,	%rdi
call	printf

// WYJSCIE Z PROGRAMU
quit_app:
movq	$0,	%rax
call	exit
