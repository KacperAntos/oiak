#include <stdio.h>
#include <string.h>

int main()
{
	char text[512];
	int wynik;
	int key = 4;

	printf( "Wpisz tekst:\n" );
	scanf( "%s", text );

	__asm__(
	"movq	$0,	%%rax\n"
	"movq	$0,	%%rcx\n"
	"movq	$0,	%%rdx\n"

	"loop:\n"
	"movb	(%0, %%rcx, 1), %%bl\n"

	"cmp	$32,	%%bl\n"
	"jl	end\n"
	"cmp	$'0',	%%bl\n"
	"jl	skip\n"
	"cmp	$'9',	%%bl\n"
	"jg	skip\n"

	"addl	%1,	%%ebx\n"
	"cmp	$'9',	%%bl\n"
	"jle	skip\n"

	"lower:\n"
	"sub	$10,	%%bl\n"
	"cmp	$'9',	%%bl\n"
	"jg	lower\n"

	"skip:\n"
	"movb	%%bl,	(%0,	%%rcx,	1)\n"
	"inc	%%rcx\n"
	"cmp	$512,	%%rcx\n"
	"jne	loop\n"

	"end:\n"
	:
	: "r" ( &text ), "r" ( key )
	: "%rax", "%rcx", "%rdx"
	);

	printf( "%s\n", text);
	return 0;
}
