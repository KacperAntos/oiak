KEY = 53
MULT = 256

.data
.text

.globl fun
.type fun, @function

fun:
pushq	%rbp
movq	%rsp,	%rbp

movq	$0,	%r9
movq	$0,	%rax

loop:
movb	(%rdi, %r9, 1),	%bl

cmp	$32,	%bl
jl	end

cmp	$'0',	%bl
jl	skip
cmp	$'9',	%bl
jg	skip

add	$KEY,	%rbx
cmp	$'9',	%bl
jle	skip

lower:
sub	$10,	%bl
cmp	$'9',	%bl
jg	lower

skip:
movb	%bl,	(%rdi, %r9, 1)
//movq	$MULT,	%r14
//mul	%r14
//add	%rbx,	%rax

inc	%r9
cmp	%r9,	%rsi
jne	loop

end:
movq	%rbp,	%rsp
popq	%rbp
ret
