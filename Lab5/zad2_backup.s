.data
clear_mask:		.short	0xFCFF
double_precision:	.short	0x0200
double_extended:	.short	0x0300
mask:			.short	0

.text

.globl	CheckFlag,	SetFlag,	Exception
.type	CheckFlag,	@function
.type	SetFlag,	@function
.type	Exception,	@function

// SPRAWDZENIE OBECNIE USTAWIONEJ FLAGI
CheckFlag:
pushq	%rbp
movq	%rsp,	%rbp

call	ReadMask
andw	$0x300,	%ax
shrw	$8,	%ax

movq	%rbp,	%rsp
popq	%rbp
ret

// USTAWIENIE NOWEJ FLAGI
SetFlag:
pushq	%rbp
movq	%rsp,	%rbp

call	ReadMask
andw	clear_mask,	%ax

cmp	$2,	%rdi
jl	end
je	double

extended:
xorw	double_extended,	%ax
jmp	end

double:
xorw	double_precision,	%ax

end:
movw	%ax,	mask
fldcw	mask

movq	%rbp,	%rsp
popq	%rbp
ret

// FUNKCJA ZCZYTUJACA OBECNA FLAGE
ReadMask:
pushq	%rbp
movq	%rsp,	%rbp

movq	$0,	%rax
fstcw	mask
fwait
movw	mask,	%ax

movq	%rbp,	%rsp
popq	%rbp
ret

Exception:
pushq	%rbp
movq	%rsp,	%rbp

//movq	$2,	%rax
//movq	$0,	%rbx
//div	%rbx

//fld1
fldz
fld1
fdiv %st(0),	%st(1)

movq	%rbp,	%rsp
popq	%rbp
ret
