#include <stdio.h>

extern double taylor(double);

int main()
{
	double x = 0;

	printf("Podaj x: ");
	scanf("%lf", &x);
	printf("e^%lf wynosi: %lf\n", x, taylor(x));

	return 0;
}
