#include <stdio.h>

extern int CheckFlag();
extern int SetFlag(int);
extern void Exception();

int main()
{
	int precision = 0;
	int k = 0;

	do
	{
		printf("\nMENU\n");
		printf("[1] Sprawdz obecna flage\n");
		printf("[2] Ustaw nowa flage\n");
		printf("[3] Podziel przez 0\n");

		printf("\n[0] Wyjdz z programu\n");
		scanf("%d", &k);

		switch(k)
		{
			case 1:
				printf("Precyzja: %d", CheckFlag());
				break;
			case 2:
				printf("Podaj nowa flage (0, 2 lub 3): ");
				scanf("%d", &precision);
				if(precision == 0 || precision == 2 || precision == 3)
					SetFlag(precision);
				break;
			case 3:
				Exception();
				break;
			default:
				k = 0;
				break;
		}
	} while(k != 0);

	return 0;
}
