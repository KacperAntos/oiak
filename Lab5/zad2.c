#include <stdio.h>

extern int CheckFlag();
extern int SetFlagToZero();
extern int SetFlagToOne();
extern void Exception();

int main()
{
	int precision = 0;
	int k = 0;

	do
	{
		printf("\nMENU\n");
		printf("[1] Sprawdz obecna flage\n");
		printf("[2] Ustaw flage na 0\n");
		printf("[3] Ustaw flage na 1\n");
		printf("[4] Podziel przez 0\n");

		printf("\n[0] Wyjdz z programu\n");
		scanf("%d", &k);

		switch(k)
		{
			case 1:
				printf("Obecna flaga: %d", CheckFlag());
				break;
			case 2:
				SetFlagToZero();
				break;
			case 3:
				SetFlagToOne();
				break;
			case 4:
				Exception();
				break;
			default:
				k = 0;
				break;
		}
	} while(k != 0);

	return 0;
}
