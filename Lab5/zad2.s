.data
mask:		.short	0
ctrlWord:	.short	0
val1:		.short	1
val2:		.short	0

.text

.globl	CheckFlag,	SetFlagToZero,	SetFlagToOne,	Exception
.type	CheckFlag,	@function
.type	SetFlagToZero,	@function
.type	SetFlagToOne,	@function
.type	Exception,	@function

// SPRAWDZENIE OBECNIE USTAWIONEJ FLAGI
CheckFlag:
pushq	%rbp
movq	%rsp,	%rbp

movq	$0,	%rax
fstcw	mask
fwait
movw	mask,	%ax
andw	$0x4,	%ax
shrw	$2,	%ax

movq	%rbp,	%rsp
popq	%rbp
ret

// USTAWIENIE FLAGI NA ZERO
SetFlagToZero:
pushq	%rbp
movq	%rsp,	%rbp

call	CheckFlag
cmp	$0,	%ax
je	endZero

fstcw	ctrlWord
xor	$4,	ctrlWord
fldcw	ctrlWord

endZero:
movq	%rbp,	%rsp
popq	%rbp
ret

// USTAWIENIE FLAGI NA JEDEN
SetFlagToOne:
pushq	%rbp
movq	%rsp,	%rbp

fstcw	ctrlWord
or	$4,	ctrlWord
fldcw	ctrlWord

movq	%rbp,	%rsp
popq	%rbp
ret

Exception:
pushq	%rbp
movq	%rsp,	%rbp

fild	val1
fdiv	val2

movq	%rbp,	%rsp
popq	%rbp
ret
