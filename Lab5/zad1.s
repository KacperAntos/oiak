.data
MAXN	= 100

top:	.short	0
bottom:	.short	0

.text

.globl taylor
.type taylor, @function

taylor:
pushq	%rbp
movq	%rsp,	%rbp

subq	$8,	%rsp
movsd	%xmm0,	(%rsp)
fldl	(%rsp)		#st(0) = x

// OPERACJE NA LICZBACH ZMIENNOPRZECINKOWYCH
movq	$0,	%r8	#Licznik 'n'
fld1			#st(0) = 1 = suma + 1
			#st(1) = x

keepCalculating:
cmp	$MAXN,	%r8
je	endOperations

// Obliczanie licznika
fld1			#st(0) = 1
			#st(1) = suma + 1
			#st(2) = x
movq	$0,	%r9

calcUpper:
cmp	%r9,	%r8
je	continueFromUpper

fmul	%st(2)		#st(0) = 1*x*x*...
			#st(1) = suma + 1
			#st(2) = x
inc	%r9
jmp	calcUpper
continueFromUpper:

// Obliczanie mianownika
fld1			#st(0) = 1
			#st(1) = x^n
			#st(2) = suma + 1
			#st(3) = x
movq	$1,	%r9
cmp	%r9,	%r8
jle	continueFromLower

movq	$1,	%r10
fld1			#st(0) = 1
			#st(1) = 1
			#st(2) = x^n
			#st(3) = suma + 1
			#st(4) = x

fld1			#st(0) = 1
			#st(1) = 1
			#st(2) = 1
			#st(3) = x^n
			#st(4) = suma + 1
			#st(5) = x

nextStep:
fadd	%st(0),	%st(1)	#st(0) = 1
			#st(1) = 1+1+1+...
			#st(2) = 1
			#st(3) = x^n
			#st(4) = suma + 1
			#st(5) = x
fstp	%st(0)
fmul	%st(0), %st(1)	#st(0) = 1
			#st(1) = 1+1+1+...
			#st(2) = 1*2*3*...
			#st(3) = x^n
			#st(4) = suma + 1
			#st(5) = x
fld1

inc	%r10
cmp	%r10,	%r8
jne	nextStep

fstp	%st(0)		#st(0) = r10 = r9 = n
			#st(1) = 1
			#st(2) = x^n
			#st(3) = suma + 1
			#st(4) = x

fstp	%st(0)		#st(0) = n!
			#st(1) = x^n
			#st(2) = suma + 1
			#st(3) = x

fxch	%st(1)		#st(0) = x^n
			#st(1) = n!
			#st(2) = suma + 1
			#st(3) = x

fdiv	%st(1)		#st(0) = (x^n) / n!
			#st(1) = n!
			#st(2) = suma + 1
			#st(3) = x

fxch	%st(1)		#st(0) = n!
			#st(1) = (x^n) / n!
			#st(2) = suma + 1
			#st(3) = x

continueFromLower:

fstp	%st(0)		#st(0) = (x^n) / n!
			#st(1) = suma + 1
			#st(2) = x

fadd	%st(0), %st(1)	#st(0) = (x^n) / n!
			#st(1) = nowaSuma + 1
			#st(2) = x

fstp	%st(0)		#st(0) = nowaSuma + 1
			#st(1) = x

inc	%r8
jmp	keepCalculating
endOperations:

fld1			#st(0) = 1
			#st(1) = suma + 1
			#st(2) = x

fsub	%st(0),	%st(1)	#st(0) = 1
			#st(1) = -suma
			#st(2) = x

fstp	%st(0)		#st(0) = -suma
			#st(1) = x

fabs			#st(0) = suma
			#st(1) = x
// KONIEC OPERACJI NA LICZBACH ZMIENNOPRZECINKOWYCH

fstpl	(%rsp)
movsd	(%rsp),	%xmm0
addq	$8,	%rsp

movq	%rbp,	%rsp
popq	%rbp
ret
