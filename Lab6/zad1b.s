.data
SYSEXIT = 60
EXIT_SUCCESS = 0

.text
.globl	FunkcjaS
.type	FunkcjaS,	@function

FunkcjaS:
pushq	%rbp
movq	%rsp,	%rbp

movq	$0,	%r8
add	$4,	%rdx

loop:
sub	$4,	%rdx

movups	(%rdi, %r8, 1),	%xmm0

movaps	%xmm0,	%xmm1
addps	%xmm0,	%xmm1

movaps	%xmm1,	(%rax, %r8, 1)

add	$16,	%r8
cmp	$0,	%rdx
jne	loop

movq	%rbp,	%rsp
popq	%rbp
ret
