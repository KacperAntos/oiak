#include <stdio.h>
#include <stdlib.h>
#include <time.h>

extern void FunkcjaS(int t[], int size);

void FunkcjaC(int t[], int size)
{
	int tn[size];

	for(int i = 0; i < size; i++)
		tn[i] = t[i] + t[i];

	return;
}

static __inline__ unsigned long long rdtsc()
{
	unsigned high;
	unsigned low;

	__asm__ __volatile__ ("rdtsc" : "=a"(low), "=d"(high));
	return( (unsigned long long)low ) | ( ( (unsigned long long)high ) << 32 );
}

int main()
{
	unsigned long long a = 0;
	unsigned long long b = 0;

	int size = 32;
	int t[size];
	for(int i = 0; i < size; i++)
		t[i] = rand() % 100;

	a = rdtsc();
	FunkcjaC(t, size);
	b = rdtsc();
	printf("Czas wykonywania funkcji w C:\t%llu\n", b - a);

	a = rdtsc();
	FunkcjaS(t, size);
	b = rdtsc();
	printf("Czas wykonywania funkcji w ASM:\t%llu\n", b - a);

	return 0;
}
